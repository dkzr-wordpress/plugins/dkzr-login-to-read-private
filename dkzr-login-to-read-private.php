<?php
/**
 * Plugin Name: Login to read Private Posts
 * Plugin URI: https://dkzr.nl/
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Description: Any role can read private posts. Also sets "anyone can register" to false.
 * Author: joostdekeijzer
 * Author URI: https://dkzr.nl
 * Version: 3
 * Text Domain: dkzr-login-to-read-private
 * Domain Path: /languages
 */

add_action( 'init', function() {
  update_option( 'users_can_register', 0 );

  if ( false === get_option( 'dkzr-login_to_read_private', false ) ) {
    $roles = [ 'administrator', 'editor', 'author', 'contributor', 'subscriber' ];
    foreach ( $roles as $role ) {
      $role = get_role( $role );
      if ( empty( $role ) ) {
        continue;
      }

      $role->add_cap( 'read_private_posts' );
      $role->add_cap( 'read_private_pages' );
    }

    update_option( 'dkzr-login_to_read_private', 1 );
  }

  //wp_set_script_translations( 'wp-editor', 'default', plugin_dir_path( __FILE__ ) . 'languages' );
} );

add_action( 'admin_head', function() {
  global $pagenow;
  if ( 'options-general.php' == $pagenow ) {
    echo '<style type="text/css">label[for=users_can_register] {opacity: 0.5;} label[for=users_can_register], input#users_can_register {pointer-events: none;} label[for=users_can_register]:after {content:" (Disabled by plugin)";}</style>';
  }
} );

add_filter( 'wp_get_nav_menu_items', function( $items, $menu, $args ) {
  if ( current_user_can( 'read_private_posts' ) || current_user_can( 'read_private_pages' ) ) {
    return $items;
  }

  foreach( $items as $key => $item ) {
    if ( isset( $item->object_id ) && 'private' == get_post_status( $item->object_id ) ) {
      unset( $items[$key] );
    }
  }
  return $items;
}, 10, 3 );

add_action( 'parse_query', function( &$wp_query ) {
  if ( ! is_admin() && ( current_user_can( 'read_private_posts' ) || current_user_can( 'read_private_pages' ) ) ) {
    if ( isset( $wp_query->query_vars['post_status'] ) && is_string( $wp_query->query_vars['post_status'] ) ) {
      $wp_query->query_vars['post_status'] = [$wp_query->query_vars['post_status']];
    }

    if ( empty( $wp_query->query_vars['post_status'] ) ) {
      $wp_query->query_vars['post_status'] = ['publish'];
    }

    $wp_query->query_vars['post_status'][] = 'private';

    if ( isset( $wp_query->query['post_status'] ) && is_string( $wp_query->query['post_status'] ) ) {
      $wp_query->query['post_status'] = [$wp_query->query['post_status']];
    }

    if ( empty( $wp_query->query['post_status'] ) ) {
      $wp_query->query['post_status'] = ['publish'];
    }

    $wp_query->query['post_status'][] = 'private';
  }
} );
